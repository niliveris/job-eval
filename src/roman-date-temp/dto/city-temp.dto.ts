import { ApiProperty } from '@nestjs/swagger';

export class CityTempDto {
  @ApiProperty({ description: 'Miminum temperature', type: 'number' })
  min: number;

  @ApiProperty({ description: 'Miminum temperature', type: 'number' })
  max: number;
}
