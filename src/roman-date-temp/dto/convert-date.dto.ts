import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export default class ConverDateDto {
  @ApiProperty({
    description:
      'date to convert in dd/mm/yyyy format. The year must be includes between 1 and 3999',
    type: 'string',
  })
  @IsString()
  date: string;
}
