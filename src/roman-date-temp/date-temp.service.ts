import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import * as moment from 'moment';
import { lastValueFrom } from 'rxjs';
import { CityTempDto } from './dto/city-temp.dto';
import { LocationDayResult } from './dto/LocationDayResult.dto';
import { LocationResult } from './dto/LocationResult.dto';

@Injectable()
export class DateTempService {
  constructor(private readonly httpService: HttpService) {}

  /**
   * Search location by name
   * @param name city name
   * @returns {LocationResult | null} return location if exist or null
   */
  async getCityId(name: string): Promise<LocationResult | null> {
    if (!name || name === '') return null;
    const resp$ = this.httpService.get<LocationResult[]>(
      `${process.env.WEATHER_API_URL}/api/location/search`,
      {
        params: {
          query: name,
        },
      },
    );

    const response = await lastValueFrom(resp$);

    if (response.data && response.data.length > 0) {
      return response.data[0];
    }

    return null;
  }

  /**
   * Return location date temps
   * @param id {string} location id
   * @param city {string} city name
   * @param date {string} date to search temps for
   * @returns {CityTempDto} temps cals at date
   */
  async getCityTemp(id: number, date: string): Promise<CityTempDto | null> {
    if (!id || !date)
      throw Error('You must provid correct id, city, and date params');

    const mdate = moment(date, 'DD-MM-YYYY');
    const dateLimit = moment().add(10, 'days');

    if (!mdate.isBetween('2013-01-01', dateLimit)) return null;

    const dateElems = date.split('/');
    const resp$ = this.httpService.get<LocationDayResult[]>(
      `${process.env.WEATHER_API_URL}/api/location/${id}/${dateElems[2]}/${dateElems[1]}/${dateElems[0]}`,
    );

    const response = await lastValueFrom(resp$);

    if (response.data && response.data.length > 0) {
      return {
        min: response.data[0].min_temp ?? null,
        max: response.data[0].max_temp ?? null,
      };
    }

    return null;
  }
}
