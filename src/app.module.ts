import { Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { RomanConvertService } from './roman-date-temp/roman-convert.service';
import { RomanDateTempModule } from './roman-date-temp/roman-date-temp.module';

@Module({
  imports: [ConfigModule.forRoot(), RomanDateTempModule],
  controllers: [],
  providers: [
    {
      provide: 'APP_PIPE',
      useFactory: () =>
        new ValidationPipe({
          whitelist: true,
          transform: true,
          forbidNonWhitelisted: true,
          transformOptions: {
            enableImplicitConversion: true,
          },
        }),
    },
    RomanConvertService,
  ],
})
export class AppModule {}
