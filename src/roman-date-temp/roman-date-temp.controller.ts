import {
  BadRequestException,
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { DateTempService } from './date-temp.service';
import ConverDateDto from './dto/convert-date.dto';
import RomanDateTempsDTO from './dto/roman-date.dto';
import { RomanConvertService } from './roman-convert.service';

@ApiTags('Roman Date temps')
@Controller('roman-date-temp')
export class RomanDateTempController {
  constructor(
    private readonly romanConvertService: RomanConvertService,
    private readonly dateTempService: DateTempService,
  ) {}

  @ApiOperation({
    description:
      'Convert a date roman format and return min and max temps for Paris at this date',
  })
  @ApiOkResponse({
    description:
      'The date has been converted and if avaiable, a temperature for Paris has been add',
    type: RomanDateTempsDTO,
  })
  @ApiBadRequestResponse({
    description: 'date is missing or incorrect',
  })
  @HttpCode(HttpStatus.OK)
  @Post()
  async convert(
    @Body() convertDateDto: ConverDateDto,
  ): Promise<RomanDateTempsDTO> {
    try {
      const { date } = convertDateDto;
      const roman_date = this.romanConvertService.convertDateToRoman(date);
      const { woeid } = await this.dateTempService.getCityId('paris');
      const temps = await this.dateTempService.getCityTemp(woeid, date);
      return {
        roman_date,
        temperatures: temps,
      };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
