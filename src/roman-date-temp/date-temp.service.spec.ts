import { HttpService } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { DateTempService } from './date-temp.service';

export type MockService<T = any> = Partial<Record<keyof T, jest.Mock>>;

describe('DateTempService', () => {
  let service: DateTempService;
  let httpMock: MockService<HttpService>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot()],
      providers: [
        DateTempService,
        {
          provide: HttpService,
          useValue: {
            get: jest.fn(() =>
              of({
                data: [],
              }),
            ),
          },
        },
      ],
    }).compile();

    service = module.get<DateTempService>(DateTempService);
    httpMock = module.get(HttpService);
  });

  test('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('has getCityId method', () => {
    test('that should return null if no name is provided', async () => {
      const location = await service.getCityId(undefined);
      expect(location).toBeNull();
    });
    test('that should return null if an empty name is provided', async () => {
      const location = await service.getCityId('');
      expect(location).toBeNull();
    });
    test('that should return an LocationResult', async () => {
      try {
        httpMock.get.mockReturnValue(
          of({ data: [{ title: 'Paris', woeid: 123 }] }),
        );

        const location = await service.getCityId('paris');
        expect(location.title).toBe('Paris');
        expect(location.woeid).toBe(123);
      } catch (error) {
        console.log('error', error);
      }
    });
  });

  describe('has getCityTemp method', () => {
    test('that should throw error if datas are missing', async () => {
      await expect(service.getCityTemp(1, null)).rejects.toThrow(
        'You must provid correct id, city, and date params',
      );
      await expect(
        service.getCityTemp(undefined, '23/01/1989'),
      ).rejects.toThrow('You must provid correct id, city, and date params');
    });
    test('that should not send request and return null if date is less than 2013', async () => {
      const httpSpy = jest.spyOn(httpMock, 'get');
      const temp = await service.getCityTemp(123, '11/04/2012');
      expect(httpSpy).not.toHaveBeenCalled();
      expect(temp).toBeNull();
    });
    test('that should not send request and return null if date is more than 10 days in the future', async () => {
      const httpSpy = jest.spyOn(httpMock, 'get');
      const temp = await service.getCityTemp(123, '30/04/2022');
      expect(httpSpy).not.toHaveBeenCalled();
      expect(temp).toBeNull();
    });
    test('that should return city temps', async () => {
      try {
        httpMock.get.mockReturnValue(
          of({
            data: [
              {
                min_temp: 1,
                max_temp: 2,
              },
            ],
          }),
        );
        const temps = await service.getCityTemp(123, '11/04/2022');
        expect(temps.min).toBe(1);
        expect(temps.max).toBe(2);
      } catch (error) {
        console.log('error', error);
      }
    });
  });
});
