import { Test, TestingModule } from '@nestjs/testing';
import { RomanConvertService } from './roman-convert.service';

describe('RomanConvertService', () => {
  let service: RomanConvertService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RomanConvertService],
    }).compile();

    service = module.get<RomanConvertService>(RomanConvertService);
  });

  test('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('has a numberToRoman function', () => {
    test('that should throw an error if the input number is not between 1 and 3999', () => {
      expect(() => {
        service.numberToRoman(0);
      }).toThrow('you can only pass numbers between 1 and 3999');
      expect(() => {
        service.numberToRoman(4000);
      }).toThrow('you can only pass numbers between 1 and 3999');
    });
    test('that should return roman number in string', () => {
      try {
        const romanNumber = service.numberToRoman(1989);
        expect(romanNumber).toEqual('MCMLXXXIX');
      } catch (error) {
        console.log('error', error);
      }
    });
  });

  describe('has a convertDateToRoman function', () => {
    test('that should throw an error if the input date format is not valid', () => {
      expect(() => {
        service.convertDateToRoman('23-01-1989');
      }).toThrow('you must provide a date in dd/mm/yyyy format');
    });
    test('that should return roman number in string', () => {
      try {
        const romanDate = service.convertDateToRoman('23/01/1989');
        expect(romanDate).toEqual('XXIII/I/MCMLXXXIX');
      } catch (error) {
        console.log('error', error);
      }
    });
  });
});
