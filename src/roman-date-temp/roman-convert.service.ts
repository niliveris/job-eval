import { Injectable } from '@nestjs/common';

@Injectable()
export class RomanConvertService {
  // HACK can be externalized
  private A = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
  private R = [
    'M',
    'CM',
    'D',
    'CD',
    'C',
    'XC',
    'L',
    'XL',
    'X',
    'IX',
    'V',
    'IV',
    'I',
  ];
  private Alength = this.A.length;

  /**
   * Convert Integer to roman Number
   * @param number Number between 1 and 3999
   * @returns Roman number
   */
  public numberToRoman(number: number): string {
    if (number < 1 || number > 3999) {
      throw Error('you can only pass numbers between 1 and 3999');
    }

    let x = parseInt(number.toString(), 10) || 1;
    let str = '';

    for (let i = 0; i < this.Alength; ++i) {
      while (x >= this.A[i]) {
        x -= this.A[i];
        str += this.R[i];
      }

      if (x == 0) {
        break;
      }
    }

    return str;
  }

  /**
   * Format date to roman date
   * @param date date in dd/mm/yyyy format by default of
   * @returns date with roman format
   */
  public convertDateToRoman = (date: string): string => {
    const sep = '/';
    const datePattern = /[0-3][0-9]\/[0-1][0-9]\/[0-9]{4}/g;

    if (!datePattern.test(date))
      throw 'you must provide a date in dd/mm/yyyy format';

    const dateElems = date.split(sep);
    const romanDateEleme = dateElems.map((rde) => {
      return this.numberToRoman(parseInt(rde, 10));
    });
    return romanDateEleme.join(sep);
  };
}
