// DTO from metaweather
export interface LocationResult {
  title: string;
  location_type: string;
  woeid: number;
  latt_long: string;
}
