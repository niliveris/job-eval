import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('Roman Date Temp Controller (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/roman-date-temp (POST)', () => {
    test('valid date for convert but invalid for temp must return converted to roman', () => {
      return request(app.getHttpServer())
        .post('/roman-date-temp')
        .expect(200)
        .expect({ roman_date: 'XXIII/I/MCMLXXXIX', temperatures: null })
        .send({ date: '23/01/1989' });
    });

    test('valid date for convert and temp must return roman date and related temperatures', async () => {
      const response = await request(app.getHttpServer())
        .post('/roman-date-temp')
        .expect(200)
        .send({ date: '11/04/2022' });

      expect(response.body.roman_date).toEqual('XI/IV/MMXXII');
      expect(response.body.temperatures).toBeDefined();
      expect(response.body.temperatures.min).toBeDefined();
      expect(response.body.temperatures.max).toBeDefined();
    });

    test('invalid date must return an error', () => {
      return request(app.getHttpServer())
        .post('/roman-date-temp')
        .expect(400)
        .expect({
          statusCode: 400,
          message: 'you must provide a date in dd/mm/yyyy format',
          error: 'Bad Request',
        })
        .send({ date: '23/01-1989' });
    });

    test('missing date must return an validation error', () => {
      return request(app.getHttpServer())
        .post('/roman-date-temp')
        .expect(400)
        .expect({
          statusCode: 400,
          message: ['date must be a string'],
          error: 'Bad Request',
        })
        .send();
    });
  });
});
