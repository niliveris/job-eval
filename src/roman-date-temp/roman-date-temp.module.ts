import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { DateTempService } from './date-temp.service';
import { RomanConvertService } from './roman-convert.service';
import { RomanDateTempController } from './roman-date-temp.controller';

@Module({
  imports: [HttpModule],
  controllers: [RomanDateTempController],
  providers: [RomanConvertService, DateTempService],
})
export class RomanDateTempModule {}
