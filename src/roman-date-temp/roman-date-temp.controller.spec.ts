import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { DateTempService } from './date-temp.service';
import { RomanConvertService } from './roman-convert.service';
import { RomanDateTempController } from './roman-date-temp.controller';

describe('RomanDateTempController', () => {
  let controller: RomanDateTempController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, ConfigModule.forRoot()],
      controllers: [RomanDateTempController],
      providers: [RomanConvertService, DateTempService],
    }).compile();

    controller = module.get<RomanDateTempController>(RomanDateTempController);
  });

  test('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
