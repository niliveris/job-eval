import { ApiProperty } from '@nestjs/swagger';
import { CityTempDto } from './city-temp.dto';

export default class RomanDateTempsDTO {
  @ApiProperty({ description: 'Converted roman date', type: 'string' })
  roman_date: string;

  @ApiProperty({
    description: 'Object of temperature of the city at the date',
    type: CityTempDto,
  })
  temperatures: CityTempDto;
}
